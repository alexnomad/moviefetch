use std::{env, time::Instant};

fn main() {
    let debug: bool;
    let rust_debug_var = env::var("DEBUG");
    match rust_debug_var {
        Ok(var) => {
            if var.to_ascii_uppercase() == "TRUE" {
                debug = true;
            } else {
                debug = false;
            }
        }
        Err(_) => debug = false,
    }

    let mut query: String = String::new();
    for i in 1..env::args().len() {
        query.push_str(&env::args().nth(i).unwrap_or("".to_owned()));
    }

    let mut start_time = Instant::now();

    let imdb_url = String::from("https://www.imdb.com");
    let mut response =
        reqwest::blocking::get(format!("{}{}{}{}", &imdb_url, "/find?q=", &query, "&s=tt"))
            .unwrap()
            .text()
            .unwrap();
    if debug {
        println!("first request successful, now scraping...")
    }
    let mut document = scraper::Html::parse_document(&response);
    let mut selector = scraper::Selector::parse(r#"td.result_text>a"#).unwrap();
    let href = document
        .select(&selector)
        .next()
        .unwrap()
        .value()
        .attr("href")
        .unwrap();

    if debug {
        let first_requst_time = start_time.elapsed();
        println!(
            "first request and scrape took {}ms",
            first_requst_time.as_millis()
        );
        start_time = Instant::now();
    }

    response = reqwest::blocking::get(format!("{}{}", &imdb_url, href))
        .unwrap()
        .text()
        .unwrap();
    if debug {
        println!("second request successful, now scraping...")
    }

    document = scraper::Html::parse_document(&response);
    selector = scraper::Selector::parse(r#"span.sc-7ab21ed2-1"#).unwrap();
    let rating = document.select(&selector).next().unwrap().inner_html();
    selector = scraper::Selector::parse(r#"h1.sc-b73cd867-0"#).unwrap();
    let name = document.select(&selector).next().unwrap().inner_html();
    selector = scraper::Selector::parse(r#"a.sc-8c396aa2-1"#).unwrap();
    let year = document.select(&selector).next().unwrap().inner_html();

    if debug {
        let second_request_time = start_time.elapsed();
        println!(
            "second requet and scrape took {}ms",
            second_request_time.as_millis()
        );
    }

    println!("{} ({})", name, year);
    for i in 0..10 {
        let m = rating.parse::<f32>().unwrap() - i as f32;
        if m >= 1.0 {
            print!("");
        } else if m < 1.0 && m > 0.0 {
            print!("");
        } else {
            print!("");
        }
    }
    print!(" {}/10\n", rating)
}
